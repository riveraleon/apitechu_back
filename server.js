var express = require('express');
var cors = require('cors');
var requestJSON = require('request-json');
var user_file = require('./file/user.json');
var bodyParser = require('body-parser');
var car_file = require('./file/car.json');
var config = require('./global');
var userController = require('./controller/userController');
var authController = require('./controller/authController');


var URL_BASE = config.variables.URL_BASE;
var app = express();
let port = process.env.port || 3000;

app.use(cors({origin: '*'}))
app.use(bodyParser.json())


var response = new Object();

app.get(URL_BASE + 'cars', function (req, res) {
    response.message = 'Correct Collection Cars';
    response.code = res.statusCode;
    response.data = car_file
    res.send(response);
});

app.get(URL_BASE + 'cars/:id', function (req, res) {
    let idSearch = req.params.id - 1;
    let respo = user_file[idSearch]
    response.code = res.statusCode;


    if (respo != null) {
        response.data = respo
        res.send(response);
    } else {
        let meensjae = 'usuario no existe';
        response.code = '404'
        response.data = meensjae
        res.send(response);
    }

});

 /* 
// GET users consumiendo API REST de mLab
app.get(URL_BASE + 'prueba',
    function (req, res) {
        console.log("GET /colapi/v3/users");
        var httpClient = requestJSON.createClient('https://api.mlab.com/api/1/databases/techu51db/collections/');
        console.log("Cliente HTTP mLab creado.");
        var queryString = 'f={"_id":0}&';
        httpClient.get('user?apiKey=NQCR6_EMDAdqyM6VEWg3scF_k32uwvHF',
            function (err, respuestaMLab, body) {
                var response = {};
                if (err) {
                    response = { "msg": "Error obteniendo usuario." }
                    res.status(500);
                } else {
                    if (body.length > 0) {
                        console.log(body.length);
                        response = body;
                    } else {
                        response = { "msg": "Ningún elemento 'user'." }
                        res.status(404);
                    }
                }
                res.send(response);
            });
    });

app.post(URL_BASE + 'usercreate',
    function (req, res) {
        var httpClient = requestJSON.createClient('https://api.mlab.com/api/1/databases/techu51db/collections/');
        httpClient.get('user?apiKey=NQCR6_EMDAdqyM6VEWg3scF_k32uwvHF',
            function (err, respuestaMLab, body) {
                console.log(body);
                newID = body.length + 1;
                var newUser = {
                    'id_user': newID,
                    'username': req.body.username,
                    'password': req.body.password,
                    'email': req.body.email

                };
                httpClient.post('user?apiKey=NQCR6_EMDAdqyM6VEWg3scF_k32uwvHF', newUser,
                    function (err, respuestaMLab, body) {
                        console.log(body);
                        res.status(201);
                        res.send(body);

                    });

            });
    });

 /* 
//PUT users con parámetro 'id'
app.put(URL_BASE + 'usersput/:id',
    function (req, res) {
        var id = req.params.id;
        var queryStringID = 'q={"id":' + id + '}&';
        var clienteMlab = requestJSON.createClient('https://api.mlab.com/api/1/databases/techu51db/collections/');
        clienteMlab.get('user?' + queryStringID + 'apiKey=NQCR6_EMDAdqyM6VEWg3scF_k32uwvHF',
            function (error, respuestaMLab, body) {
                var cambio = '{"$set":' + JSON.stringify(req.body) + '}';
                console.log(req.body);
                console.log(cambio);
                clienteMlab.put(baseMLabURL + 'user?' + queryStringID + apikeyMLab, JSON.parse(cambio),
                    function (error, respuestaMLab, body) {
                        console.log("body:" + body); // body.n devuelve 1 si pudo hacer el update
                        //res.status(200).send(body);
                        res.send(body);
                    });
            });
    });
// Petición PUT con id de mLab (_id.$oid)
app.put(URL_BASE + 'usersmLab/:id',
    function (req, res) {
        var id = req.params.id;
        let userBody = req.body;
        let eliminar = (req.query.eliminar == undefined ?  false :req.query.eliminar)
        var queryString = 'q={"id_user":' + id + '}&';
        var httpClient = requestJSON.createClient('https://api.mlab.com/api/1/databases/techu51db/collections/');
        httpClient.get('user?' + queryString + 'apiKey=NQCR6_EMDAdqyM6VEWg3scF_k32uwvHF',
            function (err, respuestaMLab, body) {
                let response = body[0];
                console.log(body);
                let updatedUser;
                //Actualizo campos del usuario
                if(eliminar){
                     updatedUser = [{}];
                }else{
                     updatedUser = {
                        "id_user": req.body.id_user,
                        "username": req.body.username,
                        "email": req.body.email,
                        "password": req.body.password
                    };
                }
                //Otra forma simplificada (para muchas propiedades)
                // var updatedUser = {};
                // Object.keys(response).forEach(key => updatedUser[key] = response[key]);
                // Object.keys(userBody).forEach(key => updatedUser[key] = userBody[key]);
                // PUT a mLab
                console.log(response)
                if (response == undefined) {
                    response = {
                        "msg": "No se encontro usuario."
                    }
                    res.send(response);

                }
                else {
                    httpClient.put('user/' + response._id.$oid + '?' + 'apiKey=NQCR6_EMDAdqyM6VEWg3scF_k32uwvHF', updatedUser,
                        function (err, respuestaMLab, body) {
                            var response = {};
                            if (err) {
                                response = {
                                    "msg": "Error actualizando usuario."
                                }
                                res.status(500);
                            } else {
                                if (body.length > 0) {
                                    response = body;
                                } else if(eliminar){
                                    response = {
                                        "msg": "Usuario eliminado correctamente."
                                    }
                                    res.status(200);
                                }
                                 else {
                                    response = {
                                        "msg": "Usuario actualizado correctamente."
                                    }
                                    res.status(200);
                                }
                            }
                            res.send(response);
                        });
                }

            });
    });

  app.put(URL_BASE + 'usersmLab/:id',
    function (req, res) {
        var id = req.params.id;
        let userBody = req.body;
        let eliminar = (req.query.eliminar == undefined ?  false :req.query.eliminar)
        var queryString = 'q={"id_user":' + id + '}&';
        var httpClient = requestJSON.createClient('https://api.mlab.com/api/1/databases/techu51db/collections/');
        httpClient.get('user?' + queryString + 'apiKey=NQCR6_EMDAdqyM6VEWg3scF_k32uwvHF',
            function (err, respuestaMLab, body) {
                let response = body[0];
                console.log(body);
                let updatedUser;
                //Actualizo campos del usuario
                if(eliminar){
                     updatedUser = [{}];
                }else{
                     updatedUser = {
                        "id_user": req.body.id_user,
                        "username": req.body.username,
                        "email": req.body.email,
                        "password": req.body.password
                    };
                }
                //Otra forma simplificada (para muchas propiedades)
                // var updatedUser = {};
                // Object.keys(response).forEach(key => updatedUser[key] = response[key]);
                // Object.keys(userBody).forEach(key => updatedUser[key] = userBody[key]);
                // PUT a mLab
                console.log(response)
                if (response == undefined) {
                    response = {
                        "msg": "No se encontro usuario."
                    }
                    res.send(response);

                }
                else {
                    httpClient.put('user?' + queryString + 'apiKey=NQCR6_EMDAdqyM6VEWg3scF_k32uwvHF', updatedUser,
                        function (err, respuestaMLab, body) {
                            var response = {};
                            if (err) {
                                response = {
                                    "msg": "Error actualizando usuario."
                                }
                                res.status(500);
                            } else {
                                if (body.length > 0) {
                                    response = body;
                                } else if(eliminar){
                                    response = {
                                        "msg": "Usuario eliminado correctamente."
                                    }
                                    res.status(200);
                                }
                                 else {
                                    response = {
                                        "msg": "Usuario actualizado correctamente."
                                    }
                                    res.status(200);
                                }
                            }
                            res.send(response);
                        });
                }

            });
    });*/




app.listen(port, function () {
    console.log('Example app listening on port 3000!');
});
app.options('*', cors());

app.post(URL_BASE + "login", authController.login);
app.post(URL_BASE + "logout/:id", authController.logout);

//app.get(URL_BASE + "users", userController.getUser);
app.post(URL_BASE + "users", userController.createUser);
app.get(URL_BASE + "users", userController.user);
app.delete(URL_BASE + "users", userController.userDelete);


//app.get(URL_BASE + "login/", userController.getUsersLogueado);
//app.get(URL_BASE + "login/:id", userController.isLogin);


