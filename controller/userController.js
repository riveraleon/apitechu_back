const config = require('../global');
const fs = require('fs');
const message_error = config.variables.message_error;

function getAllLimitUser(req,res){
    let pos = 0;
    var respuesta = new Array();
    fs.readFile(config.variables.file_user, 'utf8', function(err, contents) {
     if(err)
     {
         console.log(err);
         res.send(message_error);
     }
     else{
         var data = JSON.parse(contents);
         var limit = (req.query.limit == undefined ?  data.length :req.query.limit);

         while(limit > pos ){
           
             respuesta.push(data[pos]);
             pos++;
         }
         res.send(respuesta);
     }
 });
}

function getUserLogeuados (req,res){
    let pos = 0;
    var respuesta = new Array();
    fs.readFile(config.variables.file_user, 'utf8', function(err, contents) {
     if(err)
     {
         res.send(message_error);
     }
     else{
         var data = JSON.parse(contents);
         while(data.length > pos ){
             if( data[pos].logged== true){         
                 respuesta.push(data[pos]);
             }
             pos++;
         }
         console.log("pos "+ pos);
         console.log(respuesta);
         if(respuesta.length>0){
            res.send(respuesta);
         }
         else{
            res.send({"message": "No hay usuarios loguedos"});
         }
     }
 });
}

function userCreate (req,res){
    var httpClient = requestJSON.createClient(config.variables.api_context);
        httpClient.get('user?'+ config.variables.api_key,
            function (err, respuestaMLab, body) {
                console.log(body);
                newID = body.length + 1;
                var newUser = {
                    'id_user': newID,
                    'username': req.body.username,
                    'password': req.body.password,
                    'email': req.body.email

                };
                httpClient.post('user?'+ config.variables.api_key, newUser,
                    function (err, respuestaMLab, body) {
                        console.log(body);
                        res.status(201);
                        res.send(body);

                    });

            });
}

function users (req,res){
    var httpClient = requestJSON.createClient(config.variables.api_context);
        httpClient.get('user?'+ config.variables.api_key,
            function (err, respuestaMLab, body) {
                console.log(body);
                res.send(body);
            });
}

function userDelete (req,res){
    var httpClient = requestJSON.createClient(config.variables.api_context);
        httpClient.get('user?'+ config.variables.api_key,
            function (err, respuestaMLab, body) {
                console.log(body);
                res.send(body);
            });
}




function isUserLogeo(req,res){
    let pos = 0;
    var respuesta = new Array();
    fs.readFile(config.variables.file_user, 'utf8', function(err, contents) {
     if(err)
     {
         res.send(message_error);
     }
     else{
         var data = JSON.parse(contents);
         let pos = 0;
         let idUsuario = req.params.id;
         let respuesta = {"msg":"Usuario no Loggeado"};
         let login = false;
         while(!login && data.length > pos ){
            if( data[pos].id == idUsuario  && data[pos].logged== true){         
                respuesta = {"msg":"Usuario Loggeado"};
                 login = true;
 
            }else{
                pos++;
            }
        }
        res.send(respuesta);
     }
 });
}

module.exports.getUser = getAllLimitUser;
module.exports.getUsersLogueado = getUserLogeuados;
module.exports.isLogin = isUserLogeo;
module.exports.createUser = userCreate;
module.exports.user = users;
module.exports.userDelete = userDelete;