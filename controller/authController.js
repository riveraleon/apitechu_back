const config = require('../global');
const fs = require('fs');
var requestJSON = require('request-json');
const message_error = config.variables.message_error;

/*function login (req, res){
    let loggin = false;
    let pos = 0;
    let respuesta = {"msg":"Usuario No Loggeado"};
    fs.readFile(config.variables.file_user, 'utf8', function(err, contents) {
     if(err)
     {
         res.send(message_error);
     }
     else{
         var data = JSON.parse(contents);
         while(!loggin && data.length > pos ){
                if(data[pos].email == req.body.email &&  data[pos].paswword == req.body.password ){         
                    loggin = true;
                    data[pos].logged = true;
                    config.utilWriteFile(data);
                    respuesta = {"msg":"Usuario Loggeado"};
                }else{
                    pos++;
                }
            }
            console.log("pos "+ pos);
            console.log(respuesta);
            res.send(respuesta);
     }
 });
}*/

function login (req, res){

    var email = req.body.email;
    var password = req.body.password;
    var query = 'q={"email":"' + email + '","paswword":"' + password + '"}&'
    var httpClient = requestJSON.createClient(config.variables.api_context);
    httpClient.get('user?' + query + config.variables.api_key,
            function (err, respuestaMLab, body) {
                console.log(body)
                let putBody = '{"$set":{"logged":true}}'
                httpClient.put('user?q={"id_user": ' + body[0].id_user + '}&' + config.variables.api_key, JSON.parse(putBody),
                    function (err, respuestaMLab, body) {
                        console.log(body);
                        res.status(201);
                        let message = new Object();
                        message.mensaje = "logueado";
                        res.send(message)
                    });
            });
 };

 function logout (req, res){

    var id = req.params.id;
    var httpClient = requestJSON.createClient(config.variables.api_context);
    let putBody = '{"$set":{"logged":false}}'
    httpClient.put('user?q={"id_user": ' + id + '}&' + config.variables.api_key, JSON.parse(putBody),
        function (err, respuestaMLab, body) {
            console.log(body);
            res.status(201);
            let message = new Object();
            message.mensaje = "deslogueado";
            res.send(message)
        });
 };



/* 
function logout (req, res){
    let logout = false;
    let pos = 0;
    let respuesta = {"msg":"Usuario No Loggeado"};
    fs.readFile(config.variables.file_user, 'utf8', function(err, contents) {
     if(err)
     {
         res.send(message_error);
     }
     else{
         var data = JSON.parse(contents);
         let idUsuario = req.params.id;
         while(!logout && data.length > pos ){

             if(data[pos].id==idUsuario && data[pos].logged== true){
              logout = true;
              data[pos].logged = false;
              config.utilWriteFile(data);
              respuesta = {"msg":"Logout correcto"};
             }else{
                 pos++;
             }
         }
         console.log("pos "+ pos);
         console.log(respuesta);
         res.send(respuesta);
     }
 });
}*/
module.exports={
    login,
    logout
  };