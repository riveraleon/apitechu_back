#Imagen Docker Base/Inicial
FROM node:latest

#crear directorio del contenedor Docker
WORKDIR /docker-apitechu

#Copia archivos del proyecto en el directorio del contenedor
ADD . /docker-apitechu

#Exponer  puerto del contenedor (mismo definido en nuestra API)
EXPOSE 4000

#Lanzar comandos para ejecutar nuestra app
CMD ["npm","run","start-prod"]
#sudo docker run -p 4000:4000 -t hola-mundo
#sudo docker build -t hola-mundo .
#CMD ["npm","start"]
